import { Recipe } from '../components/recipes/recipe.model';
import { Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Subject } from 'rxjs';

@Injectable()
export class RecipeService {
    recipesChanged = new Subject<Recipe[]>();
    private recipes: Recipe[] = [
        new Recipe(
            'test1', 
            'testing', 
            'https://6.viki.io/image/43a591c2322f41e7a4b5c1fb2215cb8a.jpeg?s=900x600&e=t', 
            [
                new Ingredient('Meat', 1),
                new Ingredient('Milk', 2),
            ]
        ),
        new Recipe(
            'test2', 
            'testing', 
            'https://6.viki.io/image/43a591c2322f41e7a4b5c1fb2215cb8a.jpeg?s=900x600&e=t', 
            [
                new Ingredient('Buns', 2)
            ]
        )
    ];

    constructor(private shoppingListService: ShoppingListService) {}

    getRecipes() {
        return [...this.recipes]
    }

    getRecipe(index: number) {
        return this.recipes[index];
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.shoppingListService.addIngredients(ingredients)
    }

    addRecipe(recipe: Recipe) {
        this.recipes.push(recipe);
        this.recipesChanged.next([...this.recipes])
    }

    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
        this.recipesChanged.next([...this.recipes])
    }

    deleteRecipe(index: number) {
        this.recipes.splice(index, 1);
        this.recipesChanged.next([...this.recipes])
    }
}